
#/usr/bin/python

import smbus
import math
import csv
import re

# Power management registers
power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c

bus = smbus.SMBus(1) # or bus = smbus.SMBus(1) for Revision 2 boards
address = 0x68       # This is the address value read via the i2cdetect command

# Now wake the 6050 up as it starts in sleep mode
bus.write_byte_data(address, power_mgmt_1, 0)


def read_byte(adr):
    return bus.read_byte_data(address, adr)

def read_word(adr):
    high = bus.read_byte_data(address, adr)
    low = bus.read_byte_data(address, adr+1)
    val = (high << 8) + low
    return val

def read_word_2c(adr):
    val = read_word(adr)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val

def dist(a,b):
    return math.sqrt((a*a)+(b*b))

def get_y_rotation(x,y,z):
    radians = math.atan2(x, dist(y,z))
    return -math.degrees(radians)

def get_x_rotation(x,y,z):
    radians = math.atan2(y, dist(x,z))
    return math.degrees(radians)

if __name__ == "__main__":
    bus = smbus.SMBus(1) # or bus = smbus.SMBus(1) for Revision 2 boards
    address = 0x68       # This is the address value read via the i2cdetect command

    # Now wake the 6050 up as it starts in sleep mode
    bus.write_byte_data(address, power_mgmt_1, 0)
    
    with open('data.csv') as f:
        reader = csv.reader(f)
        gyro_avg = next(reader)
        accel_avg = next(reader)

        gyro_avg, accel_avg = ''.join(gyro_avg).split(), ''.join(accel_avg).split()
        gyro_avg = [float(i) for i in gyro_avg]
        accel_avg = [float(i) for i in accel_avg]
        print gyro_avg, accel_avg


    print "gyro data before calibration"
    gyro_xout = read_word_2c(0x43)

    print "gyro_xout: ", gyro_xout

    print "gyro data after calibration"
    print "---------------------------"

    gyro_xout = read_word_2c(0x43) - gyro_avg[0]
    gyro_yout = read_word_2c(0x45) - gyro_avg[1]
    gyro_zout = read_word_2c(0x47) - gyro_avg[2]

    print "gyro_xout: ", gyro_xout, " scaled: ", (gyro_xout / 131)
    print "gyro_yout: ", gyro_yout, " scaled: ", (gyro_yout / 131)
    print "gyro_zout: ", gyro_zout, " scaled: ", (gyro_zout / 131)

    print
    print "accelerometer data"
    print "------------------"

    accel_xout = read_word_2c(0x3b) - accel_avg[0]
    accel_yout = read_word_2c(0x3d) - accel_avg[1]
    accel_zout = read_word_2c(0x3f) - accel_avg[2]

    accel_xout_scaled = accel_xout / 16384.0
    accel_yout_scaled = accel_yout / 16384.0
    accel_zout_scaled = accel_zout / 16384.0

#    accel_xout_scaled = accel_xout_scaled / 10
#    accel_yout_scaled = accel_yout_scaled / 10
#    accel_zout_scaled = accel_zout_scaled / 10

   # print("%.2f" % round(a,2))
    print "accel_xout: ", format(accel_xout, '.2f'), " scaled: ", accel_xout_scaled
    print "accel_yout: ", accel_yout, " scaled: ", accel_yout_scaled
    print "accel_zout: ", accel_zout, " scaled: ", accel_zout_scaled

    print "x rotation: " , get_x_rotation(accel_xout_scaled, accel_yout_scaled, accel_zout_scaled)
    print "y rotation: " , get_y_rotation(accel_xout_scaled, accel_yout_scaled, accel_zout_scaled)

#-----
def initialize():
    bus = smbus.SMBus(1) # or bus = smbus.SMBus(1) for Revision 2 boards
    address = 0x68    
    bus.write_byte_data(address, power_mgmt_1, 0)
    print ("Init OK")

def fetch_values():
    
    gyro_xout = read_word_2c(0x43)
    gyro_yout = read_word_2c(0x45)
    gyro_zout = read_word_2c(0x47)

    gyro = (gyro_xout, gyro_yout, gyro_zout)
    
    accel_xout = read_word_2c(0x3b)
    accel_yout = read_word_2c(0x3d)
    accel_zout = read_word_2c(0x3f)

    accel_xout_scaled = accel_xout / 2048.0
    accel_yout_scaled = accel_yout / 2048.0
    accel_zout_scaled = accel_zout / 2048.0
    
    accel_scaled = (accel_xout_scaled, accel_yout_scaled, accel_zout_scaled)
    if gyro or accel_scaled is not None:
        print ("fetch OK")
    else:
        print ("fetch FAIL")
    return gyro, accel_scaled

